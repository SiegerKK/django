from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render


# Create your views here.
from django.template import loader

from django.conf import settings # import the settings file
from myapp.models import Order, User, Truck, Driver, Cargo
from myapp.forms import UserLoginForm, UserRegisterForm, ProfileSettingsForm, ProfileSettingsFormPOST, \
	OrderTakeForm, OrderForm, OrderFormPOST, OrderFormEdit, \
	CargoForm, CargoFormPOST, CargoEditForm, \
	TruckForm, TruckFormPOST, TruckEditForm, \
	DriverForm, DriverFormPOST, DriverEditForm


def index(request):
	return render(request, "index.html")

def orders(request):
	user = []
	if request.COOKIES.get("user_login", None):
		user = User.objects.get(login__exact=request.COOKIES['user_login'])

	args = {'Orders': Order.objects.all(), 'User': user } 
	return render(request, "orders.html", args)

def ordersTake(request, order_id):
	user = User.objects.get(login__exact=request.COOKIES['user_login'])
	order = Order.objects.get(id__exact=order_id)
	if request.method == 'POST':
		form = OrderFormPOST(request.POST)
		order.taker = user
		order.truck = Truck.objects.get(id__exact=form['truck'].value())
		order.save()
		return HttpResponseRedirect('/profile')

	trucks = Truck.objects.filter(owner=user.id)
	form = OrderTakeForm(trucks = trucks)
	return render(request, "orderTake.html", {'form': form})

def orderReject(request, order_id):
	order = Order.objects.get(id__exact=order_id)
	order.taker = None
	order.truck = None
	order.save()
	return HttpResponseRedirect('/profile')

def orderDelete(request, order_id):
	user = User.objects.get(login__exact=request.COOKIES['user_login']) 
	order = Order.objects.get(id__exact=order_id)
	if user == order.maker:
		Order.objects.get(id__exact=order_id).delete()
		return HttpResponseRedirect('/profile')

def orderFinished(request, order_id):
	order = Order.objects.get(id__exact=order_id)
	order.is_finished = True
	order.save()
	return HttpResponseRedirect('/profile')

def login(request):
	if request.COOKIES.get('user_login', None):
		return HttpResponseRedirect('/')

	error = False
	if request.method == 'POST':
		form = UserLoginForm(request.POST)
		login = form['login'].value()
		password = form['password'].value()
		if (User.objects.filter(login=login).exists()):
			user = User.objects.get(login__exact=login)
			if password == user.password:
				response = HttpResponseRedirect('/')
				response.set_cookie('user_login', login, max_age = None, expires = None)
				return response
			else:
				error = True
		else:
			error = True

	form = UserLoginForm()
	return render(request, "user/login.html", {'form': form, 'error': error})

def register(request):
	if request.COOKIES.get("user_login", None):
		return HttpResponseRedirect('/')

	error = False
	if request.method == 'POST':
		form = UserRegisterForm(request.POST)
		userLogin = form['login'].value()
		password = form['password'].value()
		name = form['name'].value()
		description = form['description'].value()

		user = User()
		user.login = userLogin
		user.password = password
		user.name = name
		user.description = description
		user.is_admin = False

		if User.objects.filter(login=userLogin).exists():
			error = True
		else:
			user.save()
			return HttpResponseRedirect('/login')

	form = UserRegisterForm()
	return render(request, "user/register.html", {'form': form, 'error': error})


def orderAdd(request):
	if not request.COOKIES.get('user_login', None):
		return HttpResponseRedirect('/')

	error = False
	user = User.objects.get(login__exact=request.COOKIES['user_login'])
	if request.method == 'POST':
		form = OrderFormPOST(request.POST)
		name = form['name'].value()
		deadline = form['deadline'].value()
		address_to = form['address_to'].value()
		address_from = form['address_from'].value()
		price = form['price'].value()
		cargo = Cargo.objects.get(id__exact=form['cargo'].value())

		order = Order()
		order.name = name
		order.deadline = deadline
		order.address_to = address_to
		order.address_from = address_from
		order.price = price
		order.cargo = cargo
		order.is_finished = False
		order.maker = user
		order.save()
		return HttpResponseRedirect('/profile')

	cargos = Cargo.objects.filter(owner=user.id)
	form = OrderForm(cargos = cargos)
	return render(request, "user/orderAdd.html", {'form': form, 'error': error})

def orderEdit(request, order_id):
	if not request.COOKIES.get('user_login', None):
		return HttpResponseRedirect('/')

	error = False
	user = User.objects.get(login__exact=request.COOKIES['user_login'])
	if Truck.objects.filter(owner=user.id).exists():
		user.trucks = Truck.objects.filter(owner=user.id)
	else:
		user.trucks = {}
	order = Order.objects.get(id__exact=order_id)
	if request.method == 'POST':
		form = OrderFormPOST(request.POST)
		if order.maker == user:
			name = form['name'].value()
			deadline = form['deadline'].value()
			address_to = form['address_to'].value()
			address_from = form['address_from'].value()
			price = form['price'].value()
			cargo = Cargo.objects.get(id__exact=form['cargo'].value())

			order.name = name
			order.deadline = deadline
			order.address_to = address_to
			order.address_from = address_from
			order.price = price
			order.cargo = cargo
			order.is_finished = False
			order.maker = user

		if order.taker == user:
			order.truck = Truck.objects.get(id__exact=form['truck'].value())

		order.save()
		return HttpResponseRedirect('/profile')

	order.cargos = Cargo.objects.filter(owner=user.id)
	form = OrderFormEdit(order = order, user = user)
	return render(request, "user/orderEdit.html", {'form': form, 'order': order})

def logout(request):
	response = HttpResponseRedirect('/')
	response.delete_cookie('user_login')
	return response

def profile(request):
	if not request.COOKIES.get('user_login', None):
		return HttpResponseRedirect('/')

	args = {}
	args['User'] = User.objects.get(login__exact=request.COOKIES['user_login'])
	args['Orders'] = Order.objects.all()
	args['Trucks'] = Truck.objects.all()
	args['Drivers'] = Driver.objects.all()
	args['Cargos'] = Cargo.objects.all()
	return render(request, "user/profile.html", args)

def profileSettings(request):
	if not request.COOKIES.get("user_login", None):
		return HttpResponseRedirect('/')

	error = False
	user = User.objects.get(login__exact=request.COOKIES['user_login'])
	if request.method == 'POST':
		form = ProfileSettingsFormPOST(request.POST)
		login = form['login'].value()

		if login != request.COOKIES['user_login'] and User.objects.filter(login=login).exists():
			error = True
		else:
			name = form['name'].value()
			password = form['password'].value()
			description = form['description'].value()

			user.name = name
			user.login = login
			user.password = password
			user.description = description
			user.save()
			response = HttpResponseRedirect('/profile/settings')
			# Update cookie if login changed
			if login != request.COOKIES['user_login']:
				response.set_cookie('user_login', login, max_age = None, expires = None)

			return response

	form = ProfileSettingsForm(user = user)
	return render(request, "user/profileSettings.html", {'form': form, 'error': error})


def cargoAdd(request):
	if not request.COOKIES.get('user_login', None):
		return HttpResponseRedirect('/')

	error = False
	owner = User.objects.get(login__exact=request.COOKIES['user_login'])
	if request.method == 'POST':
		form = CargoFormPOST(request.POST)
		name = form['name'].value()
		description = form['description'].value()
		weight = form['weight'].value()

		cargo = Cargo()
		cargo.name = name
		cargo.description = description
		cargo.weight = weight
		cargo.owner = owner
		cargo.save()
		return HttpResponseRedirect('/profile')

	form = CargoForm()
	return render(request, "user/cargoAdd.html", {'form': form, 'error': error})


def cargoEdit(request, cargo_id):
	if not request.COOKIES.get('user_login', None):
		return HttpResponseRedirect('/')

	error = False
	owner = User.objects.get(login__exact=request.COOKIES['user_login'])
	cargo = Cargo.objects.get(id__exact=cargo_id)
	if request.method == 'POST':
		form = CargoFormPOST(request.POST)
		name = form['name'].value()
		description = form['description'].value()
		weight = form['weight'].value()

		cargo.name = name
		cargo.description = description
		cargo.weight = weight
		cargo.owner = owner
		cargo.save()
		return HttpResponseRedirect('/profile')

	form = CargoEditForm(cargo = cargo)
	return render(request, "user/cargoEdit.html", {'form': form, 'error': error})

def cargoDelete(request, cargo_id):
	if not request.COOKIES.get('user_login', None):
		return HttpResponseRedirect('/')

	Cargo.objects.get(id__exact=cargo_id).delete()
	return HttpResponseRedirect('/profile')

def driverAdd(request):
	if not request.COOKIES.get('user_login', None):
		return HttpResponseRedirect('/')

	error = False
	owner = User.objects.get(login__exact=request.COOKIES['user_login'])
	if request.method == 'POST':
		form = DriverFormPOST(request.POST)
		name = form['name'].value()
		phone_number = form['phone_number'].value()

		driver = Driver()
		driver.name = name
		driver.phone_number = phone_number
		driver.owner = owner
		driver.save()
		return HttpResponseRedirect('/profile')

	form = DriverForm()
	return render(request, "user/driverAdd.html", {'form': form, 'error': error})


def driverEdit(request, driver_id):
	if not request.COOKIES.get('user_login', None):
		return HttpResponseRedirect('/')

	error = False
	owner = User.objects.get(login__exact=request.COOKIES['user_login'])
	driver = Driver.objects.get(id__exact=driver_id)
	if request.method == 'POST':
		form = DriverFormPOST(request.POST)
		name = form['name'].value()
		phone_number = form['phone_number'].value()

		driver.name = name
		driver.phone_number = phone_number
		driver.owner = owner
		driver.save()
		return HttpResponseRedirect('/profile')

	# return HttpResponse(driver.phone_number)
	form = DriverEditForm(driver = driver)
	return render(request, "user/driverEdit.html", {'form': form, 'error': error})

def driverDelete(request, driver_id):
	if not request.COOKIES.get('user_login', None):
		return HttpResponseRedirect('/')

	Driver.objects.get(id__exact=driver_id).delete()
	return HttpResponseRedirect('/profile')

def truckAdd(request):
	if not request.COOKIES.get('user_login', None):
		return HttpResponseRedirect('/')

	error = False
	owner = User.objects.get(login__exact=request.COOKIES['user_login'])
	if request.method == 'POST':
		form = TruckFormPOST(request.POST)
		serial_number = form['serial_number'].value()
		driver = Driver.objects.get(id__exact=form['driver'].value())

		truck = Truck()
		truck.serial_number = serial_number
		truck.driver = driver
		truck.owner = owner
		truck.save()
		return HttpResponseRedirect('/profile')

	drivers = Driver.objects.filter(owner=owner.id)
	form = TruckForm(drivers = drivers)
	return render(request, "user/truckAdd.html", {'form': form, 'error': error})


def truckEdit(request, truck_id):
	if not request.COOKIES.get('user_login', None):
		return HttpResponseRedirect('/')

	error = False
	owner = User.objects.get(login__exact=request.COOKIES['user_login'])
	truck = Truck.objects.get(id__exact=truck_id)
	if request.method == 'POST':
		form = TruckFormPOST(request.POST)
		serial_number = form['serial_number'].value()
		driver = Driver.objects.get(id__exact=form['driver'].value())

		truck.serial_number = serial_number
		truck.driver = driver
		truck.owner = owner
		truck.save()
		return HttpResponseRedirect('/profile')

	drivers = Driver.objects.filter(owner=owner.id)
	form = TruckEditForm(drivers = drivers, truck = truck)
	return render(request, "user/truckEdit.html", {'form': form, 'error': error})

def truckDelete(request, truck_id):
	if not request.COOKIES.get('user_login', None):
		return HttpResponseRedirect('/')

	Truck.objects.get(id__exact=truck_id).delete()
	return HttpResponseRedirect('/profile')
	
