from django.db import models


class User(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=500)
    login = models.CharField(max_length=50)
    password = models.CharField(max_length=50)
    # is_admin = models.BooleanField()

    def __str__(self):
        return f"{self.name}"


class Cargo(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=500)
    weight = models.FloatField()
    owner = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.name}"


class Driver(models.Model):
    name = models.CharField(max_length=50)
    phone_number = models.CharField(max_length=15)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.name}"


class Truck(models.Model):
    serial_number = models.CharField(max_length=50)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    driver = models.ForeignKey(Driver, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.serial_number}"


class Order(models.Model):
    name = models.CharField(max_length=50)
    deadline = models.DateTimeField()
    address_to = models.CharField(max_length=50)
    address_from = models.CharField(max_length=50)
    price = models.FloatField()
    is_finished = models.BooleanField()
    maker = models.ForeignKey(User, on_delete=models.CASCADE, related_name='maker', null=False)
    taker = models.ForeignKey(User, on_delete=models.CASCADE, related_name='taker', blank=True, null=True)
    truck = models.ForeignKey(Truck, on_delete=models.CASCADE, blank=True, null=True)
    cargo = models.ForeignKey(Cargo, on_delete=models.CASCADE, null=False)

    def __str__(self):
        return f"{self.name}"