from django import forms

from myapp.models import User, Cargo, Order, Driver, Truck


class UserLoginForm(forms.ModelForm):
	class Meta:
		model = User
		fields = ('login', 'password')
		widgets = {
			"login": forms.TextInput(attrs={"class": "form-control form-control-sm"}),
			"password": forms.TextInput(attrs={"class": "form-control form-control-sm", "type": "password"})
		}
class UserRegisterForm(forms.ModelForm):
	class Meta:
		model = User
		fields = ('login', 'password', 'name', 'description')
		widgets = {
			"login": forms.TextInput(attrs={"class": "form-control form-control-sm", "pattern": "^[a-zA-Z0-9]*"}),
			"password": forms.TextInput(attrs={"class": "form-control form-control-sm", "type": "password"}),
			"name": forms.TextInput(attrs={"class": "form-control form-control-sm"}),
			"description": forms.TextInput(attrs={"class": "form-control form-control-sm"})
		}
class ProfileSettingsForm(forms.Form):
	def __init__(self, user, *args, **kwargs):
		super(ProfileSettingsForm,self).__init__(*args,**kwargs)
		self.fields['name'].widget = forms.TextInput(attrs={"class": "form-control form-control-sm", "value": user.name})
		self.fields['login'].widget = forms.TextInput(attrs={"class": "form-control form-control-sm", "value": user.login})
		self.fields['password'].widget = forms.TextInput(attrs={"class": "form-control form-control-sm", "value": user.password})
		self.fields['description'].widget = forms.TextInput(attrs={"class": "form-control form-control-sm", "value": user.description})

	name = forms.CharField()
	login = forms.CharField()
	password = forms.CharField()
	description = forms.CharField()
class ProfileSettingsFormPOST(forms.ModelForm):
	class Meta:
		model = User
		fields = ('name', 'login', 'password', 'description')


class OrderForm(forms.Form):
	def __init__(self, cargos, *args, **kwargs):
		super(OrderForm, self).__init__(*args,**kwargs)
		self.fields['name'].widget = forms.TextInput(attrs={"class": "form-control form-control-sm"})
		self.fields['deadline'].widget = forms.TextInput(attrs={"class": "form-control form-control-sm", "type": "datetime-local"})
		self.fields['address_from'].widget = forms.TextInput(attrs={"class": "form-control form-control-sm"})
		self.fields['address_to'].widget = forms.TextInput(attrs={"class": "form-control form-control-sm"})
		self.fields['price'].widget = forms.NumberInput(attrs={"class": "form-control form-control-sm"})
		self.fields['cargo'] = forms.ChoiceField(choices=tuple([(cargo.id, cargo.name) for cargo in cargos]), widget=forms.Select(attrs={'class':'form-select form-select-sm', "required": True}))

	name = forms.CharField()
	deadline = forms.CharField()
	address_from = forms.CharField()
	address_to = forms.CharField()
	price = forms.CharField()
	cargo = forms.CharField()
class OrderFormEdit(forms.Form):
	def __init__(self, order, user, *args, **kwargs):
		super(OrderFormEdit, self).__init__(*args,**kwargs)
		self.fields['name'].widget = forms.TextInput(attrs={"class": "form-control form-control-sm", "value": order.name, "hidden": user != order.maker})
		self.fields['name'].label = '' if user != order.maker else 'Name'
		self.fields['deadline'].widget = forms.TextInput(attrs={"class": "form-control form-control-sm", "type": "datetime-local", "hidden": user != order.maker})
		self.fields['deadline'].label = '' if user != order.maker else 'Deadline'
		datetime = str(order.deadline)
		date = datetime[0:10]
		time = datetime[11:19]
		self.fields['deadline'].initial = date+'T'+time
		self.fields['address_from'].widget = forms.TextInput(attrs={"class": "form-control form-control-sm", "value": order.address_from, "hidden": user != order.maker})
		self.fields['address_from'].label = '' if user != order.maker else 'Address From'
		self.fields['address_to'].widget = forms.TextInput(attrs={"class": "form-control form-control-sm", "value": order.address_to, "hidden": user != order.maker})
		self.fields['address_to'].label = '' if user != order.maker else 'Address To'
		self.fields['price'].widget = forms.NumberInput(attrs={"class": "form-control form-control-sm", "value": order.price, "hidden": user != order.maker})
		self.fields['price'].label = '' if user != order.maker else 'Price'
		self.fields['cargo'] = forms.ChoiceField(choices=tuple([(cargo.id, cargo.name) for cargo in order.cargos]), initial=order.cargo.id, widget=forms.Select(attrs={'class':'form-select form-select-sm', "hidden": user != order.maker}))
		self.fields['cargo'].label = '' if user != order.maker else 'Cargo'
		if order.truck:
			self.fields['truck'] = forms.ChoiceField(choices=tuple([(truck.id, truck.serial_number) for truck in user.trucks]), initial=order.truck.id, widget=forms.Select(attrs={'class':'form-select form-select-sm', "hidden": user != order.taker}))
			self.fields['truck'].label = '' if user != order.taker else 'Truck'
		else:
			self.fields['truck'].widget = forms.Select(attrs={"hidden": True})
			self.fields['truck'].label = ''

	name = forms.CharField()
	deadline = forms.CharField()
	address_from = forms.CharField()
	address_to = forms.CharField()
	price = forms.CharField()
	cargo = forms.CharField()
	truck = forms.CharField()
class OrderFormPOST(forms.ModelForm):
    class Meta:
        model = Order
        fields = ('name', 'deadline', 'address_from', 'address_to', 'price', 'cargo', 'truck')
class OrderTakeForm(forms.Form):
	def __init__(self, trucks, *args, **kwargs):
		super(OrderTakeForm, self).__init__(*args,**kwargs)
		self.fields['truck'] = forms.ChoiceField(choices=tuple([(truck.id, truck.serial_number) for truck in trucks]), widget=forms.Select(attrs={'class':'form-select form-select-sm', "required": True}))

	truck = forms.CharField()


class CargoForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(CargoForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget = forms.TextInput(attrs={"class": "form-control form-control-sm"})
        self.fields['description'].widget = forms.TextInput(attrs={"class": "form-control form-control-sm"})
        self.fields['weight'].widget = forms.NumberInput(attrs={"class": "form-control form-control-sm", "step": 0.01})

    name = forms.CharField()
    description = forms.CharField()
    weight = forms.CharField()
class CargoFormPOST(forms.ModelForm):
    class Meta:
        model = Cargo
        fields = ('name', 'description', 'weight')
class CargoEditForm(forms.Form):
    def __init__(self, cargo, *args, **kwargs):
        super(CargoEditForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget = forms.TextInput(attrs={"class": "form-control form-control-sm",
                                                            "value": cargo.name})
        self.fields['description'].widget = forms.TextInput(attrs={"class": "form-control form-control-sm",
                                                                   "value": cargo.description})
        self.fields['weight'].widget = forms.NumberInput(attrs={"class": "form-control form-control-sm",
                                                                "value": cargo.weight, "step": 0.01})

    name = forms.CharField()
    description = forms.CharField()
    weight = forms.CharField()


class DriverForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(DriverForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget = forms.TextInput(attrs={"class": "form-control form-control-sm"})
        self.fields['phone_number'].widget = forms.TextInput(attrs={"class": "form-control form-control-sm", "type": "tel", "pattern": "[0-9]{3}-[0-9]{2}-[0-9]{3}"})

    name = forms.CharField()
    phone_number = forms.CharField()
class DriverFormPOST(forms.ModelForm):
    class Meta:
        model = Driver
        fields = ('name', 'phone_number')
class DriverEditForm(forms.Form):
    def __init__(self, driver, *args, **kwargs):
        super(DriverEditForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget = forms.TextInput(attrs={"class": "form-control form-control-sm",
                                                            "value": driver.name})
        self.fields['phone_number'].widget = forms.TextInput(attrs={"class": "form-control form-control-sm",
                                                                    "type": "tel",
                                                                    "pattern": "[0-9]{3}-[0-9]{2}-[0-9]{3}",
                                                                    "value": driver.phone_number})

    name = forms.CharField()
    phone_number = forms.CharField()


class TruckForm(forms.Form):
    def __init__(self, drivers, *args, **kwargs):
        super(TruckForm, self).__init__(*args, **kwargs)
        self.fields['serial_number'].widget = forms.TextInput(attrs={"class": "form-control form-control-sm"})
        self.fields['driver'] = forms.ChoiceField(choices=tuple([(driver.id, driver.name) for driver in drivers]), widget=forms.Select(attrs={'class':'form-select form-select-sm', "required": True}))

    driver = forms.CharField()
    serial_number = forms.CharField()
class TruckFormPOST(forms.ModelForm):
    class Meta:
        model = Truck
        fields = ('serial_number', 'driver')
class TruckEditForm(forms.Form):
    def __init__(self, drivers, truck, *args, **kwargs):
        super(TruckEditForm, self).__init__(*args, **kwargs)
        self.fields['serial_number'].widget = forms.TextInput(attrs={"class": "form-control form-control-sm",
                                                                     "value": truck.serial_number})
        self.fields['driver'] = forms.ChoiceField(choices=tuple([(driver.id, driver.name) for driver in drivers]),
                                                  initial=truck.driver.id,
                                                  widget=forms.Select(attrs={'class':'form-select form-select-sm'}))

    driver = forms.CharField()
    serial_number = forms.CharField()
