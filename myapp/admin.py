from django.contrib import admin

# Register your models here.
from myapp.models import User, Cargo, Order, Truck, Driver

admin.site.register(User)
admin.site.register(Cargo)
admin.site.register(Order)
admin.site.register(Truck)
admin.site.register(Driver)