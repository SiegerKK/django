from django.contrib import admin
from django.urls import path

from myapp import views

urlpatterns = [
    # Main page
    path('', views.index),
    # All orders
    path('orders', views.orders),
    # Take order
    path('orders/take/<int:order_id>', views.ordersTake),
    # Login
    path('login', views.login, name='login'),
    # Logout
    path('logout', views.logout, name='logout'),
    # Register
    path('register', views.register, name='register'),

    # User | Dashboard
    path('profile/', views.profile),
    # User | Settings
    path('profile/settings', views.profileSettings, name='profileSettings'),
    # # User | Order | Add
    path('profile/order/add', views.orderAdd, name='orderAdd'),
    # # User | Order | Edit
    path('profile/order/edit/<int:order_id>', views.orderEdit, name='orderEdit'),
    # # User | Order | Reject
    path('profile/order/reject/<int:order_id>', views.orderReject),
    # # User | Order | Delete
    path('profile/order/delete/<int:order_id>', views.orderDelete),
     # # User | Order | Finished
    path('profile/order/finished/<int:order_id>', views.orderFinished),
    # # User | Truck | Add
    path('profile/truck/add', views.truckAdd),
    # # User | Truck | Edit
    path('profile/truck/edit/<int:truck_id>', views.truckEdit),
    # # User | Truck | Delete
    path('profile/truck/delete/<int:truck_id>', views.truckDelete),
    # # User | Cargo | Add
    path('profile/cargo/add', views.cargoAdd),
    # # User | Cargo | Edit
    path('profile/cargo/edit/<int:cargo_id>', views.cargoEdit),
    # # User | Cargo | Delete
    path('profile/cargo/delete/<int:cargo_id>', views.cargoDelete),
    # # User | Driver | Add
    path('profile/driver/add', views.driverAdd),
    # # User | Driver | Edit
    path('profile/driver/edit/<int:driver_id>', views.driverEdit),
    # # User | Driver | Delete
    path('profile/driver/delete/<int:driver_id>', views.driverDelete),
]
